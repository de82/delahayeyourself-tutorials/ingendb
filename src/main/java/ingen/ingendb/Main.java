package ingen.ingendb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sam
 */
public class Main {

    public static void list(Connection connection) throws SQLException {
        String query = "SELECT * FROM dinosaurs;";

        Statement statement = connection.createStatement();

        ResultSet resultSet = statement.executeQuery(query);

        while (resultSet.next()) {
            String name = resultSet.getString("name");
            System.out.println(name);
        }

        resultSet.close();
        statement.close();
    }
    
    public static void search(Connection connection, String name) throws SQLException{
        String query = "SELECT * FROM dinosaurs WHERE name LIKE ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, name);
        
        ResultSet resultSet = preparedStatement.executeQuery();
        
        while (resultSet.next()) {
            String found_name = resultSet.getString("name");
            System.out.println(found_name);
        }

        resultSet.close();
        preparedStatement.close();
        
    }
    
    public static void insert(Connection connection, String name, String meaning) throws SQLException{
        String query = "INSERT INTO dinosaurs (name, name_meaning) VALUES(?, ?);";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, meaning);
        
        int i = preparedStatement.executeUpdate();
        System.out.println(String.format("%s row(s) updated", i));
        
        preparedStatement.close();
    }

    public static void main(String[] args) {
        String datapath = "/home/sam/Downloads/ingen.db";

        try {
            Class.forName("org.sqlite.JDBC");

            Connection connection = DriverManager.getConnection(String.format("jdbc:sqlite:%s", datapath));
            
            Main.insert(connection, "Indominus Rex", "A useless dinosaur ..");

            Main.search(connection, "Indom%");
            
            connection.close();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
